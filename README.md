xpi_gamecon : kernel module for Piboy DMT
======

xpi_gamecon is a Linux kernel module which allow to support the specific hardware of the Piboy DMT from Experimental PI :
 - fan driver
 - screen backlight
 - leds
 - controls

